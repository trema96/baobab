import java.io.StringWriter;
import java.util.*;

public class StateExplorer {
	public static void explore(List<List<Action>> userActions, Set<Integer> tupleSpace, StringWriter writer) {
		List<List<State>> states = new ArrayList<>();
		states.add(Collections.singletonList(new State(0, userActions, tupleSpace)));
		int currLayer = 0;
		List<State> nextLayer = new ArrayList<>();
		states.add(nextLayer);
		while (!states.get(currLayer).isEmpty()) {
			for (State state : states.get(currLayer)) {
				state.explore(nextLayer);
			}
			nextLayer = new ArrayList<>();
			states.add(nextLayer);
			currLayer++;
		}
		currLayer = 0;
		for (List<State> stateList : states) {
			int currId = 0;
			for (State state : stateList) {
				state.setAssignedID(currLayer + "_" + currId);
				currId++;
			}
			currLayer++;
		}
		for (List<State> stateList : states) {
			stateList.forEach((state) -> {
				writer.write(state.printed());
			});
			writer.write("\n");
		}
	}
}
