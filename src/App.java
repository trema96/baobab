import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class App {
	public static void main(String [] args) throws IOException {
		Set<Integer> tupleSpace = new HashSet<>();
		tupleSpace.add(1);
		tupleSpace.add(2);
		tupleSpace.add(3);
		FileWriter fw = new FileWriter("file.txt");
		StringWriter sw = new StringWriter();
		StateExplorer.explore(
				Arrays.asList(
						Arrays.asList(
							new InAction(1),
							new InAction(2),
							new OutAction(1),
							new OutAction(2),
							FinalAction.INSTANCE
						),
						Arrays.asList(
							new InAction(2),
							new InAction(3),
							new OutAction(2),
							new OutAction(3),
							FinalAction.INSTANCE
						),
						Arrays.asList(
							new InAction(3),
							new InAction(1),
							new OutAction(3),
							new OutAction(1),
							FinalAction.INSTANCE
						)
				),
				tupleSpace,
				sw
		);
		fw.write(sw.toString());
		fw.close();
	}
}
