import javafx.util.Pair;

import java.awt.peer.CanvasPeer;
import java.util.*;
import java.util.stream.Collectors;

public class State {
	private final int layer;
	private final List<List<Action>> userActions;
	private final Set<Integer> tupleSpace;
	private String assignedID = null;
	private final List<Pair<Pair<Integer, Action>, State>> transitions = new ArrayList<>();

	public State(int layer, List<List<Action>> userActions, Set<Integer> tupleSpace) {
		this.layer = layer;
		this.userActions = userActions;
		this.tupleSpace = tupleSpace;
	}

	public void setAssignedID(String assignedID) {
		Objects.requireNonNull(assignedID);
		this.assignedID = assignedID;
	}

	/**
	 * @param nextLayerStates
	 * 	already existing states of the next level. Newly added state will also be added to this list
	 */
	public void explore(List<State> nextLayerStates) {
		for (int i = 0; i < userActions.size(); i++) {
			if (userActions.get(i).get(0).canPerform(tupleSpace)) {
				List<List<Action>> newActions = new ArrayList<>();
				Set<Integer> newTupleSpace = new HashSet<>(tupleSpace);
				for (int j = 0; j < userActions.size(); j++) {
					if (j != i) {
						newActions.add(new ArrayList<>(userActions.get(j)));
					} else {
						List<Action> updatedActionList = new ArrayList<>(userActions.get(i));
						updatedActionList.remove(0);
						newActions.add(updatedActionList);
					}
				}
				userActions.get(i).get(0).perform(newTupleSpace);
				State newState = new State(layer + 1, newActions, newTupleSpace);
				int newStateIndex = nextLayerStates.indexOf(newState);
				if (newStateIndex < 0) {
					nextLayerStates.add(newState);
				} else {
					newState = nextLayerStates.get(newStateIndex);
				}
				transitions.add(new Pair<>(new Pair<>(i, userActions.get(i).get(0)), newState));
			}
		}
	}

	public String printed() {
		StringBuilder res = new StringBuilder("state s" + assignedID);
		if (transitions.isEmpty()) {
			res.append(" <<Ending>>");
		}
		res.append(":");
		userActions.forEach((actionList) -> {
			res.append(" ");
			res.append(actionList.stream().map(Action::getDesc).collect(Collectors.joining(".")));
			res.append("\\n ||");
		});
		res.append(" ");
		if (tupleSpace.size() > 0) {
			res.append(tupleSpace.stream().sorted().map((tuple) -> "t" + StringUtils.getSubscript(tuple)).collect(Collectors.joining(" \u222a ")));
		} else {
			res.append("\u2205");
		}
		res.append(System.lineSeparator());
		transitions.forEach((transition) -> {
			res.append("s");
			res.append(assignedID);
			res.append(" --> s");
			res.append(transition.getValue().assignedID);
			res.append(": ");
			res.append(transition.getKey().getValue().getPerformDesc(transition.getKey().getKey() + 1));
			res.append(System.lineSeparator());
		});
		return res.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		State state = (State) o;
		return layer == state.layer &&
				Objects.equals(userActions, state.userActions) &&
				Objects.equals(tupleSpace, state.tupleSpace);
	}

	@Override
	public int hashCode() {

		return Objects.hash(layer, userActions, tupleSpace);
	}
}
