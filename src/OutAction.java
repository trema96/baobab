import java.util.Objects;
import java.util.Set;

public class OutAction implements Action {
	private final int target;

	public OutAction(int target) {
		this.target = target;
	}

	@Override
	public boolean canPerform(Set<Integer> tupleSpace) {
		return true;
	}

	@Override
	public String getDesc() {
		return "out(t" + StringUtils.getSubscript(target) + ")";
	}

	@Override
	public String getPerformDesc(int performId) {
		return "out(t" + StringUtils.getSubscript(target) + ")\\n[OUT" + StringUtils.getSubscript(performId) + "]";
	}

	@Override
	public void perform(Set<Integer> tupleSpace) {
		tupleSpace.add(target);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		OutAction outAction = (OutAction) o;
		return target == outAction.target;
	}

	@Override
	public int hashCode() {
		return Objects.hash(target);
	}
}
