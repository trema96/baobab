public final class StringUtils {
	private StringUtils() {}

	public static String tBarra() {
		return "t\u0304";
	}

	public static String getSubscript(int i) {
		if (i < 0 || i > 9) {
			throw new IllegalArgumentException();
		}
		return new String(Character.toChars(Integer.parseInt("2080", 16) + i));
	}
}
