import java.util.Objects;
import java.util.Set;

public class InAction implements Action {
	private final int target;

	public InAction(int target) {
		this.target = target;
	}
	@Override
	public boolean canPerform(Set<Integer> tupleSpace) {
		return tupleSpace.contains(target);
	}

	@Override
	public String getDesc() {
		return "in(" + StringUtils.tBarra() + StringUtils.getSubscript(target) + ")";
	}

	@Override
	public String getPerformDesc(int performId) {
		return "in(t" + StringUtils.getSubscript(target) + ")\\n[IN" + StringUtils.getSubscript(performId) + "]";
	}

	@Override
	public void perform(Set<Integer> tupleSpace) {
		tupleSpace.remove(target);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		InAction inAction = (InAction) o;
		return target == inAction.target;
	}

	@Override
	public int hashCode() {
		return Objects.hash(target);
	}
}
