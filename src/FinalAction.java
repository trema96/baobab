import java.util.Set;

public class FinalAction implements Action {
	public static FinalAction INSTANCE = new FinalAction();

	private FinalAction(){}

	@Override
	public boolean canPerform(Set<Integer> tupleSpace) {
		return false;
	}

	@Override
	public String getDesc() {
		return "0";
	}

	@Override
	public String getPerformDesc(int performId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void perform(Set<Integer> tupleSpace) {
		throw new UnsupportedOperationException();
	}
}
