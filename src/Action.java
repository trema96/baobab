import java.util.Set;

public interface Action {
	boolean canPerform(Set<Integer> tupleSpace);
	String getDesc();
	String getPerformDesc(int performId);
	void perform(Set<Integer> tupleSpace);
}
